<?php
$title = 'Datos Usuario';
require_once '../shared/header.php';
$marca = filter_input(INPUT_POST, 'marca', FILTER_SANITIZE_STRING);
$modelo = filter_input(INPUT_POST, 'modelo', FILTER_SANITIZE_STRING);
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require_once '../shared/db.php';
    $vehiculo_model->create($marca, $modelo);
    return header('Location: /vehiculos');
}
?>
<div class="container">
    
</div>
